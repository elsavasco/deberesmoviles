package SingletonDP

object User{
    var name = "Inmobiliaria"

    init {
        println("Singleton invoked")
    }

    fun printName() = println(name)
}