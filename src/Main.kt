
import BridgeDP.RemoteControl
import BridgeDP.VacuumCleaner
import BridgeDP.Switch
import BridgeDP.Appliance
import BridgeDP.TV

import DecoratorDP.*
import PrototypeDP.*
import SingletonDP.*
import ObserverDP.*
import StateDP.*

fun main (args:Array<String>){

	/*var tvRemoteControl = RemoteControl(appliance = TV())
    tvRemoteControl.turnOn()
    var fancyVacuumCleanerRemoteControl = RemoteControl(appliance = VacuumCleaner())
    fancyVacuumCleanerRemoteControl.turnOn()*/
	
	
	/*var tipoDePizza=basePizza("masa pan de yuca")
	println(tipoDePizza.hacerPizza());
	
	var veg=rellenoCarne(tipoDePizza,"champiniones");
	println(veg.hacerPizza())
	
	var veg1=rellenoVegetariana(tipoDePizza,"albaca, tofu");
	println(veg1.hacerPizza())
	*/
	/*val bike = Bike()
    val basicBike = bike.clone()
    val advancedBike = makeJaguar(basicBike);
	
	println("Modelo inicial: "+bike.model)
	println("Modelo clonado: "+basicBike.model)
    println("Modelo mejorado: " + advancedBike.model)*/
	
	/*User.printName()
    User.name = "Carroceria"
    User.printName()*/
	
	///Observer
	/*val observableObject= ObservableObject(PrintingTextChangedListener())
	observableObject.text= "Hola"
	observableObject.text= "Listo"*/
	
	//State
	val stateContext = AlertStateContext()
    stateContext.alert()
    stateContext.alert()
    stateContext.setState(Silent())
    stateContext.alert()
    stateContext.alert()
    stateContext.setState(Sound())
    stateContext.alert()
	
}

 fun makeJaguar(basicBike: Bike): Bike {
    basicBike.makeAdvanced()
    return basicBike
}