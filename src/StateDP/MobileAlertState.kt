package StateDP

interface MobileAlertState {
	fun alert(ctx: AlertStateContext)
}