package PrototypeDP

open class Bike : Cloneable {
    private var cilinders: Int = 0
    private var bikeType: String? = null
    public var model: String? = null
    
	private set
	 init {
        bikeType = "Standard"
        model = "Moto de Paseo"
        cilinders = 4
    }

    public override fun clone(): Bike {
        return Bike()
    }

    fun makeAdvanced() {
        bikeType = "Advanced"
        model = "Moto de Pista"
        cilinders = 6
    }
}


