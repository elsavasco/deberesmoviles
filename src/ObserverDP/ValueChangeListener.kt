package ObserverDP

interface ValueChangeListener {
	fun onValueChanged(newValue:String)
}