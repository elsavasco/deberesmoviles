package BuilderDP

abstract class ConstructorPlan(val plan:Plan) {
	
	public fun getPlan():Plan{
		return plan;
	}
	
	public abstract fun setNombre();
	public abstract fun setipo();
	public abstract fun setPrecio();
}