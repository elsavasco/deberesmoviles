package BridgeDP

class RemoteControl (override var appliance: Appliance) : Switch {
	override fun turnOn() = appliance.run()
}