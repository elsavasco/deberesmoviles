package BridgeDP

interface Switch {
	var appliance: Appliance
    fun turnOn()
}