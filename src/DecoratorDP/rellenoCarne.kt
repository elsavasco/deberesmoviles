package DecoratorDP

class rellenoCarne (private val pizza:Pizza, private val relleno: String):Pizza {
	override fun hacerPizza(): String{
		return pizza.hacerPizza()+ " "+ relleno
	}
} 