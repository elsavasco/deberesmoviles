package DecoratorDP

class rellenoVegetariana(private val pizza:Pizza, private val relleno: String):Pizza{
	override fun hacerPizza():String{
		return pizza.hacerPizza()+" "+relleno
	}
}